#ifndef ORT_NETWORK_H
#define ORT_NETWORK_H

#include <QtCore>

#include <ort_serverlist.h>
#include <../NetLogger/netlogger.h>


class ortConfiguration;


class ortNetwork : public QObject
{
    Q_OBJECT
public:
    ortNetwork();

    ortConfiguration* configInstance;
    void setConfigInstance(ortConfiguration* instance);

    bool prepare();

    bool openConnection(bool fallback=false);
    void closeConnection();    
    bool transferQueueFiles();

    bool reconnectToMatchingServer(QString requiredServerType);

    QString connectCmd;
    QString serverPath;
    QString disconnectCmd;
    QString fallbackConnectCmd;
    int     connectTimeout;

    QString appPath;

    QDir queueDir;
    QDir serverDir;
    QDir serverTaskDir;

    ortServerList serverList;
    QString       selectedServer;
    QString       currentServer;

    QString errorReason;

    QStringList fileList;

    QString currentFilename;
    qint64  currentFilesize;

    void releaseFile();
    bool removeFile();
    bool verifyTransfer();
    bool getFileToProcess(int index);
    bool copyFile();

    void cleanLocalQueueDir();

    NetLogger netLogger;
};


#endif // ORT_NETWORK_H
